import { IsInt, IsNotEmpty, IsPositive, Length } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  @IsInt()
  @IsPositive()
  age: number;

  @IsNotEmpty()
  @Length(10, 10, { message: 'telephone must be equal to 10 characters' })
  tel: string;

  @IsNotEmpty()
  @Length(1, 1, { message: 'gender must be M or F' })
  gender: string;
}
